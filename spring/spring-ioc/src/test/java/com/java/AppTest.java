package com.java;


import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("ioc.xml");
        Book book = (Book)applicationContext.getBean("book");
        System.out.println(book.getTitle());
    }
}
