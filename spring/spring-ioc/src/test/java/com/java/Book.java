package com.java;


/**
 * @Auther: yangzhen
 * @Date: 2019/3/16 16:01
 * @Description:
 */
public class Book {
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
