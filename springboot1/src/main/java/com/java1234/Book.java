package com.java1234;

/**
 * @Auther: yangzhen
 * @Date: 2019/3/12 23:51
 * @Description:
 */
public class Book {

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
