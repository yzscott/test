package com.java1234;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Auther: yangzhen
 * @Date: 2019/3/8 00:10
 * @Description:
 */
@RestController
@RequestMapping("hello")
public class Hello {

    @RequestMapping("test")
    public String test(Book book){
        return "test";
    }
}
